[Appearance]
ColorScheme=One_Dark
Font=mononoki Nerd Font Mono,12,-1,5,50,0,0,0,0,0
LineSpacing=1
TabColor=27,30,32,0

[Cursor Options]
CursorShape=1

[General]
DimWhenInactive=true
InvertSelectionColors=false
Name=HotEarth
Parent=FALLBACK/
ShowTerminalSizeHint=true
TerminalMargin=1

[Keyboard]
KeyBindings=default

[Terminal Features]
VerticalLine=false
