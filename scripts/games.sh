#!/bin/bash
#This requires "dialog" to be installed!
while true; do
    read -p "Shall we install some Games? [Y/n] " yn
    case $yn in
        [Yy]* ) 
		onoff=off
cmd=(dialog --output-fd 1 --separate-output --extra-button --extra-label 'Select All' --cancel-label 'Select None' --checklist 'Choose the Games to install:' 0 0 0)
load-dialog () {
    options=(
                1 'Flare: Empyrean Campaign (2D action role playing game)' $onoff
                2 '0 A.D. (Real-Time Strategy Game of Ancient Warfare)' $onoff
                3 'TanksOfFreedom (Turn Based Strategy in Isometric Pixel Art)' $onoff
                4 'Mindustry (A sandbox tower-defense game)' $onoff
                5 'OpenRA (3 real-time strategy games)' $onoff
                6 'MegaGlest (real time strategy game)' $onoff
                7 'Wesnoth (turn-based strategy game)' $onoff
                8 'Cap: Pirate Battleship (Battleship game)' $onoff
                9 'Shattered Pixel Dungeon (Roguelike RPG)' $onoff
                10 'Frets on Fire (Guitar Hero)' $onoff
                11 'C-Dogs SDL (Classic overhead run-and-gun game)' $onoff
                12 'Chromium B.S.U. (Fast paced, arcade-style, top-scrolling space shooter)' $onoff
                13 'Warmux (Worms-clone)' $onoff
                14 'Battle Tanks (A fun filled scrolling game with battle tanks)' $onoff
                15 'Mr Rescue (Arcade-style fire fighting game)' $onoff
                16 'Open Surge (Sonic-Clone, 2D retro platformer)' $onoff
                17 'SuperTux (Mario-Clone, jump-and-run game)' $onoff
                18 'Pingus (Lemmings-Clone)' $onoff
                19 'Not Tetris 2 (Physics-based Tetris)' $onoff
                20 'Frozen Bubble (An addictive game about frozen bubbles)' $onoff
                21 'Stunt Rally (Racing game with rally style of driving)' $onoff
                22 'Extreme Tuxracer (High speed arctic racing game)' $onoff
                23 'Sonic Robo Blast 2 Kart (Kart racing mod based on Sonic Robo Blast 2)' $onoff
                24 'Speed Dreams (3D motorsport simulation and racing game)' $onoff
                25 'SuperTuxKart (MarioKart-Clone, 3D kart racing game)' $onoff
    )
    choices=$("${cmd[@]}" "${options[@]}")
}
load-dialog
exit_code="$?"
while [[ $exit_code -ne 0 ]]; do
case $exit_code in
    1) clear; onoff=off; load-dialog;;
    3) clear; onoff=on; load-dialog;;
esac
exit_code="$?"
done
clear
for choice in $choices
do
    case $choice in
        1) flatpak install -y flathub org.flarerpg.Flare;;
        2) flatpak install -y flathub com.play0ad.zeroad;;
        3) flatpak install -y flathub in.p1x.TanksOfFreedom;;
        4) flatpak install -y flathub com.github.Anuken.Mindustry;;
        5) flatpak install -y flathub net.openra.OpenRA;;
        6) flatpak install -y flathub org.megaglest.MegaGlest;;
        7) flatpak install -y flathub org.wesnoth.Wesnoth;;
        8) flatpak install -y flathub net.tedomum.CapBattleship;;
        9) flatpak install -y flathub com.shatteredpixel.shatteredpixeldungeon;;
        10) flatpak install -y flathub net.sourceforge.fretsonfire;;
        11) flatpak install -y flathub io.github.cxong.cdogs-sdl;;
        12) flatpak install -y flathub net.sourceforge.chromium-bsu;;
        13) flatpak install -y flathub org.gna.Warmux;;
        14) flatpak install -y flathub net.sourceforge.btanks;;
        15) flatpak install -y flathub dk.tangramgames.mrrescue;;
        16) flatpak install -y flathub org.opensurge2d.OpenSurge;;
        17) flatpak install -y flathub org.supertuxproject.SuperTux;;
        18) flatpak install -y flathub org.seul.pingus;;
        19) flatpak install -y flathub net.stabyourself.nottetris2;;
        20) flatpak install -y flathub org.frozen_bubble.frozen-bubble;;
        21) flatpak install -y flathub org.tuxfamily.StuntRally;;
        22) flatpak install -y flathub net.sourceforge.ExtremeTuxRacer;;
        23) flatpak install -y flathub org.srb2.SRB2Kart;;
        24) flatpak install -y flathub org.speed_dreams.SpeedDreams;;
        25) flatpak install -y flathub net.supertuxkart.SuperTuxKart;;
    esac
done
		echo done; break;;
        [Nn]* ) echo cancelled; break;;
        "" ) reboot;;
        * ) echo "Please answer yes or no. is that so hard?";;
    esac
done



while true; do
    read -p "Shall we install some Shooter Games? [Y/n] " yn
    case $yn in
        [Yy]* ) 
		onoff=off
cmd=(dialog --output-fd 1 --separate-output --extra-button --extra-label 'Select All' --cancel-label 'Select None' --checklist 'Choose the Games to install:' 0 0 0)
load-dialog () {
    options=(
                1 'Xonotic (Multiplayer, deathmatch oriented first person shooter)' $onoff
                2 'Tremulous (Aliens vs Humans, First Person Shooter)' $onoff
                3 'OpenArena (first-person shooter)' $onoff
                4 'FreeDM (Deathmatch game based on the Doom engine)' $onoff
                5 'Sauerbraten (multiplayer & singleplayer first person shooter,)' $onoff
                6 'Red Eclipse (First-person shooter with agile gameplay)' $onoff
                7 'ET: Legacy (Wolfenstein: Enemy Territory)' $onoff
                8 'Wolfenstein: Blade of Agony (Story-driven FPS)' $onoff
                9 'Freedoom: Phase 1 (First-person shooter based on the Doom engine)' $onoff
                10 'Freedoom: Phase 2 (First-person shooter based on the Doom engine)' $onoff
                11 'Total Chaos (Survival horror)' $onoff
                12 'Unvanquished (fast paced, first person strategy game)' $onoff
                13 'Quetoo (first person shooter)' $onoff
                14 'Nexuiz Classic (multiplayer first-person shooter)' $onoff
    )
    choices=$("${cmd[@]}" "${options[@]}")
}
load-dialog
exit_code="$?"
while [[ $exit_code -ne 0 ]]; do
case $exit_code in
    1) clear; onoff=off; load-dialog;;
    3) clear; onoff=on; load-dialog;;
esac
exit_code="$?"
done
clear
for choice in $choices
do
    case $choice in
        1) flatpak install -y flathub org.xonotic.Xonotic;;
        2) flatpak install -y flathub com.grangerhub.Tremulous;;
        3) flatpak install -y flathub ws.openarena.OpenArena;;
        4) flatpak install -y flathub io.github.freedoom.FreeDM;;
        5) flatpak install -y flathub org.sauerbraten.Sauerbraten;;
        6) flatpak install -y flathub net.redeclipse.RedEclipse;;
        7) flatpak install -y flathub com.etlegacy.ETLegacy;;
        8) flatpak install -y flathub com.realm667.Wolfenstein_Blade_of_Agony;;
        9) flatpak install -y flathub io.github.freedoom.Phase1;;
        10) flatpak install -y flathub io.github.freedoom.Phase2;;
        11) flatpak install -y flathub com.moddb.TotalChaos;;
        12) flatpak install -y flathub net.unvanquished.Unvanquished;;
        13) flatpak install -y flathub org.quetoo.Quetoo;;
        14) flatpak install -y flathub com.alientrap.nexuiz-classic;;
    esac
done
		echo done; break;;
        [Nn]* ) echo cancelled; break;;
        "" ) reboot;;
        * ) echo "Please answer yes or no. is that so hard?";;
    esac
done

		
# The Dark Mod # https://www.thedarkmod.com/downloads/	
echo "##################"
echo "## The Dark Mod ##"
echo "##################"
while true; do
    read -p "Shall we install The Dark Mod? (This might take forver...)[Y/n] " yn
    case $yn in
        [Yy]* ) cd ~/ && mkdir -p ~/darkmod && wget https://update.thedarkmod.com/zipsync/tdm_installer.linux64.zip && unzip tdm_installer.linux64.zip -d ~/darkmod && rm tdm_installer.linux64.zip && cd ~/darkmod && chmod +x tdm_installer.linux64 && ./tdm_installer.linux64; 
		echo done; break;;
        [Nn]* ) echo cancelled; break;;
        "" ) reboot;;
        * ) echo "Please answer yes or no. is that so hard?";;
    esac
done





#GTK Games
if [ "$(echo $XDG_CURRENT_DESKTOP)" = GNOME ]; then
echo 
echo "##################"
echo "# Gnome detected #"
echo "##################"
while true; do
    read -p "Shall we install some GTK Games? [Y/n] " yn
    case $yn in
        [Yy]* ) 
onoff=off
cmd=(dialog --output-fd 1 --separate-output --extra-button --extra-label 'Select All' --cancel-label 'Select None' --checklist 'Choose the Gnome Games to install:' 0 0 0)
load-dialog () {
    options=(
                1 'Quadrapassel (Tetris)' $onoff
                2 'Mines (Minesweeper)' $onoff
                3 'Sudoku' $onoff
                4 'Aisleriotr (Solitaire)' $onoff
                5 'Chess' $onoff
                6 'Hitori (Puzzle Game)' $onoff
                7 'Tali (Dice Game)' $onoff
                8 'Tetravex (Reorder tiles to fit a square)' $onoff
                9 'Mahjongg' $onoff				
    )
    choices=$("${cmd[@]}" "${options[@]}")
}
load-dialog
exit_code="$?"
while [[ $exit_code -ne 0 ]]; do
case $exit_code in
    1) clear; onoff=off; load-dialog;;
    3) clear; onoff=on; load-dialog;;
esac
exit_code="$?"
done
clear
for choice in $choices
do
    case $choice in
        1) flatpak install -y flathub org.gnome.Quadrapassel;;
        2) flatpak install -y flathub org.gnome.Mines;;
        3) flatpak install -y flathub org.gnome.Sudoku;;
        4) flatpak install -y flathub org.gnome.Aisleriot;;
        5) flatpak install -y flathub org.gnome.Chess;;
        6) flatpak install -y flathub org.gnome.Hitori;;
        7) flatpak install -y flathub org.gnome.Tali;;
        8) flatpak install -y flathub org.gnome.Tetravex;;
        9) flatpak install -y flathub org.gnome.Mahjongg;;
    esac
done
		echo done; break;;
        [Nn]* ) echo cancelled; break;;
        "" ) reboot;;
        * ) echo "Please answer yes or no. is that so hard?";;
    esac
done
fi






#QT & KDE Games
if [ "$(echo $XDG_CURRENT_DESKTOP)" = KDE ]; then
echo 
echo "################"
echo "# KDE detected #"
echo "################"
while true; do
    read -p "Shall we install some QT & KDE Games? [Y/n] " yn
    case $yn in
        [Yy]* ) 
onoff=off
cmd=(dialog --output-fd 1 --separate-output --extra-button --extra-label 'Select All' --cancel-label 'Select None' --checklist 'Choose the QT/KDE Games to install:' 0 0 0)
load-dialog () {
    options=(
                1 'knights (Chess)' $onoff
                2 'wz2100 (Strategy)' $onoff
                3 'Hedgewars (Worms)' $onoff
                4 'bomber' $onoff
                5 'granatier (Bomberman)' $onoff
                6 'knavalbattle (Battleship)' $onoff
                7 'KPatience (Card Game)' $onoff
                8 'KLines' $onoff
                9 'KBounce (Arcade)' $onoff
                10 'KJumpingcube (Dice driven Tactical Game)' $onoff
                11 'KGoldrunner (Maze-threading Game)' $onoff
                12 'killbots' $onoff
                13 'KsirK (Risk)' $onoff
                14 'kbreakout (destroy bricks)' $onoff
                15 'kiriki (Dice Game)' $onoff
                16 'kmines (Minesweeper)' $onoff
                17 'KMahjongg (Mahjongg)' $onoff
                18 'ksudoku (Sudoku)' $onoff
                19 'kblocks (Tetris)' $onoff
                20 'kapman (Pacman)' $onoff
    )
    choices=$("${cmd[@]}" "${options[@]}")
}
load-dialog
exit_code="$?"
while [[ $exit_code -ne 0 ]]; do
case $exit_code in
    1) clear; onoff=off; load-dialog;;
    3) clear; onoff=on; load-dialog;;
esac
exit_code="$?"
done
clear
for choice in $choices
do
    case $choice in
        1) flatpak install -y knights;;
        2) flatpak install -y flathub net.wz2100.wz2100;;
        3) flatpak install -y flathub org.hedgewars.Hedgewars;;
        4) flatpak install -y flathub org.kde.bomber;;
        5) flatpak install -y flathub org.kde.granatier;;
        6) flatpak install -y flathub org.kde.knavalbattle;;
        7) flatpak install -y flathub org.kde.kpat;;
        8) flatpak install -y flathub org.kde.klines;;
        9) flatpak install -y flathub org.kde.kbounce;;
		10) flatpak install -y flathub org.kde.kjumpingcube;;
		11) flatpak install -y flathub org.kde.kgoldrunner;;
		12) flatpak install -y flathub org.kde.killbots;;
		13) flatpak install -y KsirK;;
		14) flatpak install -y kbreakout;;
		15) flatpak install -y kiriki;;
		16) flatpak install -y kmines;;
		17) flatpak install -y KMahjongg;;
		18) flatpak install -y flathub org.kde.ksudoku;;
		19) flatpak install -y flathub org.kde.kblocks;;
		20) flatpak install -y flathub org.kde.kapman;;
    esac
done
		echo done; break;;
        [Nn]* ) echo cancelled; break;;
        "" ) reboot;;
        * ) echo "Please answer yes or no. is that so hard?";;
    esac
done
fi
